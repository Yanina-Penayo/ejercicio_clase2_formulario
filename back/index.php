<?php
$ruta_del_archivo = "../datos_formularios.txt"; // directorio de la ubicación de la carpeta
$file = "datos_formulario.txt"; // nombre del archivo

// Verificamos si el archivo existe o lo creamos
if (!file_exists($ruta_del_archivo . $file)) {
    // Creamos el archivo y le damos permisos
    if (touch($ruta_del_archivo . $file)) {
        chmod($ruta_del_archivo . $file, 0755);
        echo "<strong>Se creó el archivo y se le dieron los permisos correspondientes</strong><br>";
    } else {
        echo "Error al crear el archivo.";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $apellido = strtolower($_POST["apellido"]);
    $nombre = strtolower($_POST["nombre"]);
    $fecha_evento = $_POST['fecha_evento'];
    $correo_electronico = $_POST['correo_electronico'];
    $fecha_de_nacimiento = $_POST['fecha_de_nacimiento'];
    $redes_sociales = $_POST['redes_sociales'];
    $perfil = $_POST['perfil'];
    $adicional = $_POST['adicional'];

    // Construimos el mensaje
    $mensaje = "Apellido: $apellido\n";
    $mensaje .= "Nombre: $nombre\n";
    $mensaje .= "Fecha de evento: $fecha_evento\n";
    $mensaje .= "Correo electrónico: $correo_electronico\n";
    $mensaje .= "Fecha de nacimiento: $fecha_de_nacimiento\n";
    $mensaje .= "Redes sociales: $redes_sociales\n";
    $mensaje .= "Perfil del inscripto: $perfil\n";
    $mensaje .= "Charla, meet o cursos a los cuales asistirá: $adicional\n";
    $mensaje .= "---------------------------------------------\n"; 
    $ruta_del_archivo_final = $ruta_del_archivo . $file;

    // Abre o crea el archivo en modo de escritura
    $archivo = fopen($ruta_del_archivo_final, "a+");

    // Escribe los datos en el archivo
    if ($archivo) {
        fwrite($archivo, $mensaje);
        fclose($archivo);
        echo "Los datos se han guardado correctamente en el archivo.";
    } else {
        echo "Error al abrir el archivo.";
    }
}
?>