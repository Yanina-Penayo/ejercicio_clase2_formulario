<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Document</title>
</head><body>
    
    <form method="POST" action="../back/index.php">

        <p style="color: red; ">*Campo requerido </p>
        <label for="apellido">Apellido:</label>
        <span style="color: red;">*</span>
        <input type="text" placeholder="Ingrese su apellido" minlength="2" id="campo1" name="apellido" required><br><br>
        

        <!--
            todos los campos de un formulario se llaman input y reciben un atributo. El atributo type sirve para diferenciar que tipo de entrada de datos tenemos. EL type text establece una caja de texto.
        -->
        
        <label for="nombre">Nombre:</label>
        <!-- La etiqueta label se utiliza para etiquetas o describir lo que se espera que ingrese el usuario en un campo de formulario. EL for se usa para asociar la etiqueta label con un elemento del formulario-->

        <span style="color: red;">*</span>
        <input type="text" placeholder="Ingrese su nombre" minlength="2"  id="campo2" name="nombre" required><br><br>
        
        <label for="fecha_evento">Fecha de evento: </label>
        <span style="color: red;">*</span><br><br>        
        <span id="cb_validation"></span>
        <input type="checkbox" id="opcionfecha1" name="opcionfecha1" ><label for="opcionfecha1" class="fecha-texto">24/05/2024</label> <br><br>
        <input type="checkbox" id="opcionfecha2" name="opcionfecha2" ><label for="opcionfecha2" class="fecha-texto">25/05/2024</label> <br><br>
        <input type="checkbox" id="opcionfecha3" name="opcionfecha3" ><label for="opcionfecha3" class="fecha-texto">26/05/2024</label> <br><br>

        <label for="correo_electronico">Correo electrónico:</label>
        <span style="color: red;">*</span>
        <input type="email" placeholder="Ingrese su correo"id="campo4" name="correo_electronico" required><br><br>

        <label for="fecha_de_nacimiento">Fecha de nacimiento:</label>
        <span style="color: red;">*</span>
        <input type="date" id="campo5" name="fecha_de_nacimiento" required min="1900-01-01"><br><br>

        <label for="redes_sociales">Redes sociales:</label>
        <input type="text" placeholder="(Opcional)" id="campo6" name="redes_sociales" ><br><br>

        <label for="perfil"> Perfil del inscripto: </label>
        <input type="text" placeholder="(Opcional)" id="campo7" name="perfil" ><br><br>

        <label for="adicional">Charla, meet o cursos a los cuales asistirá:</label>
        <input type="text" placeholder="(Opcional)" id="campo8" name="adicional" ><br><br>

        <input type="submit" value="Enviar">
        
    </form>
</body>
</html>